import * as webpack from 'webpack'
import * as path from 'path'
import MiniCssExtractPlugin from "mini-css-extract-plugin"
import HTMLWebpackPlugin from 'html-webpack-plugin'

const src = path.resolve(__dirname, 'src')
const dst = path.resolve(__dirname, 'build')

const config: webpack.Configuration = {
  resolve: {
    extensions: ['.ts', '.tsx', '.js']
  },
  devtool: 'source-map',
  entry: path.join(src, 'index.ts'),
  plugins: [
      new HTMLWebpackPlugin({
      template: path.join(src, 'index.html'),
      minify: {
        removeComments: true,
        minifyJS: true,
        collapseWhitespace: true,
        conservativeCollapse: true
      }
      }),
      new MiniCssExtractPlugin({
          filename: 'style.css'
      })
  ],
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
          use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader'],
      }, {
        test: /\.tsx?$/,
        use: 'ts-loader'
      }, {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: 'file-loader'
      }, {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: 'url-loader?limit=10000&mimetype=application/font-woff'
      }, {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: 'file-loader'
      }
    ]
  },
  output: {
    path: dst,
    publicPath: '/',
    filename: 'bundle.js'
  }
}

export default config