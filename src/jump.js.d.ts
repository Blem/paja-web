declare module 'jump.js' {
  function jump (target: string | Element | number, opts?: jump.Options): void;

  namespace jump {
    type TransitionFunc = (t: number, b: number, c: number, d: number) => number;

    interface Options {
      duration?: number;
      offset?: number;
      callback?: () => void;
      easing?: TransitionFunc;
      a11y?: boolean;
    }
  }

  export default jump
}