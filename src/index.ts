import './main.scss'
import jump from 'jump.js'

const top = document.getElementById('top-bar')
const getTopOffset = () => top.offsetHeight

const setHash = (hash: string) => {
  if (history.pushState) {
    history.pushState(null, null, hash)
  } else {
    location.hash = hash
  }
}

const navigateTo = (targetHash: string) => {

  jump(targetHash, {
    duration: 500,
    offset: -getTopOffset(),
    callback: () => setHash(targetHash)
  })
}

const init = () => {
  if (window.location.hash) {
    //chrome hack
    if ('scrollRestoration' in history) {
      history.scrollRestoration = 'manual'
    }

    const el = document.querySelector(window.location.hash)
    const yPos = window.pageYOffset.valueOf() + el.getBoundingClientRect().top - getTopOffset()
    window.scrollTo(window.pageYOffset.valueOf(), yPos)
  }

  const links = document.getElementsByTagName('a')
  for (let i = 0; i < links.length; ++i) {
    const targetName = links[i].getAttribute('href')

    if (targetName[0] === '#') {
      links[i].addEventListener('click', e => {
        e.preventDefault()
        navigateTo(targetName)
      })
    }
  }

  window.onpopstate = e => {
    if (location.hash) {
      jump(location.hash, {duration: 500, offset: -getTopOffset()})
    } else {
      jump('#uvod', {duration: 500})
    }
  }
}

document.onreadystatechange = () => {
  if (document.readyState === 'interactive') {
    init()
  }
}