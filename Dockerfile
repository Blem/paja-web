FROM node:11.7-alpine AS build
WORKDIR /app
COPY package.json .
RUN yarn install
COPY . .
RUN yarn run build

FROM nginx:1.15-alpine

COPY --from=build /app/build/* /usr/share/nginx/html/
